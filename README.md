Go package for thread-safe rate limiting (all credit goes to [bsm](https://github.com/bsm))

Get:
```
go get -u gitlab.com/maikeme/ratelimit
```

Use:
```
package main

import (
	"fmt"

	"gitlab.com/maikeme/ratelimit"
)

func main() {
	rl := ratelimit.New(10, time.Second)

  for i := 0; i < 20; i++ {
    if rl.Limit() {
      fmt.Println("Limit exceeded")
    }
  }
}
```
