package ratelimit

import (
	"sync/atomic"
	"time"
)

// RateLimiter instances are thread-safe.
type RateLimiter struct {
	Rate      uint64
	Allowance uint64
	Max       uint64
	Unit      uint64
	LastCheck uint64
}

// New creates a new rate limiter instance
func New(rate int, per time.Duration) *RateLimiter {
	nano := uint64(per)
	if nano < 1 {
		nano = uint64(time.Second)
	}
	if rate < 1 {
		rate = 1
	}

	return &RateLimiter{
		Rate:      uint64(rate),        // store the rate
		Allowance: uint64(rate) * nano, // set our allowance to max in the beginning
		Max:       uint64(rate) * nano, // remember our maximum allowance
		Unit:      nano,                // remember our unit size

		LastCheck: uint64(time.Now().UnixNano()),
	}
}

// Limit returns true if rate was exceeded
func (rl *RateLimiter) Limit() bool {
	// Calculate the number of ns that have passed since our last call
	now := uint64(time.Now().UnixNano())
	passed := now - atomic.SwapUint64(&rl.LastCheck, now)

	// Add them to our allowance
	rate := atomic.LoadUint64(&rl.Rate)
	current := atomic.AddUint64(&rl.Allowance, passed*rate)

	// Ensure our allowance is not over maximum
	if max := atomic.LoadUint64(&rl.Max); current > max {
		atomic.AddUint64(&rl.Allowance, max-current)
		current = max
	}

	// If our allowance is less than one unit, rate-limit!
	if current < rl.Unit {
		return true
	}

	// Not limited, subtract a unit
	atomic.AddUint64(&rl.Allowance, -rl.Unit)
	return false
}

// UpdateRate allows to update the allowed rate
func (rl *RateLimiter) UpdateRate(rate int) {
	atomic.StoreUint64(&rl.Rate, uint64(rate))
	atomic.StoreUint64(&rl.Max, uint64(rate)*rl.Unit)
}

// Undo reverts the last Limit() call, returning consumed allowance
func (rl *RateLimiter) Undo() {
	current := atomic.AddUint64(&rl.Allowance, rl.Unit)

	// Ensure our allowance is not over maximum
	if max := atomic.LoadUint64(&rl.Max); current > max {
		atomic.AddUint64(&rl.Allowance, max-current)
	}
}
